from django.db import models

# Create your models here.
class Planet(models.Model):
    name = models.CharField(max_length=256)
    system = models.CharField(max_length=256)
    sector = models.CharField(max_length=256)
    galactic_longitude = models.FloatField(default=0.0)
    galactic_latitude = models.FloatField(default=0.0)